#!/usr/bin/env sh

set -v
set -e

CONTAINER_NAME="$1"
CONTAINER_TEST_IMAGE="$2"

docker rm -f ${CONTAINER_NAME} 2>&1 > /dev/null || true

docker run \
    --name ${CONTAINER_NAME} \
    -e START_KEYBASE="false" \
    -dt ${CONTAINER_TEST_IMAGE} ash

# tests
docker exec -t ${CONTAINER_NAME} ash -c 'type npm'
docker exec -t ${CONTAINER_NAME} ash -c 'type chromium-browser'
docker exec -t ${CONTAINER_NAME} ash -c 'ps aux | grep -v grep | grep Xvfb'

# clean up
docker rm -f ${CONTAINER_NAME}
