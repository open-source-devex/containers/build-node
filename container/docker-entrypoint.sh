#!/usr/bin/env bash

set -e

if [[ ! -f /tmp/.X99-lock ]]; then

    # Run a Xvfb to run headless browsers
    _kill_proc() {
      kill -TERM ${xvfb}
    }

    # Setup a trap to catch SIGTERM and relay it to child processes
    trap _kill_proc SIGTERM

    XVFB_WHD=${XVFB_WHD:-1280x720x16}

    # Start Xvfb
    Xvfb :99 -ac -screen 0 ${XVFB_WHD} -nolisten tcp &
    xvfb=$!

    export DISPLAY=":99"

fi

/build-docker-entrypoint.sh "$@"
