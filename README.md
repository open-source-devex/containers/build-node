# build-node

Docker container image run NodeJS (or otherwise JS based) builds.

Headless Chrome support for running tests obtained from [docker-chromium-xvfb](https://github.com/mark-adams/docker-chromium-xvfb).
